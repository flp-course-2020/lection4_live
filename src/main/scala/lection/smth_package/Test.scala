package lection.smth_package

object TestPackageObject extends App {
  println(testStringFromPackageObject)

  def foo(implicit str: String): String = str

  println(foo)
}
