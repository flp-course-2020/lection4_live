package lection.ready

trait Show[A] {
  def show(el: A): String
}

object Show {
  def apply[A](implicit sh: Show[A]): Show[A] = sh

  implicit class ShowSyntax[A](el: A) {
    def show(implicit sh: Show[A]): String = sh.show(el)
  }

}
