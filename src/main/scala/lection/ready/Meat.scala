package lection.ready

sealed trait Meat

trait Pork extends Meat

trait Beef extends Meat

trait Chicken extends Meat

object Meat {
  implicit val showMeat = new Show[Meat] {
    override def show(el: Meat): String = el match {
      case el: Pork    => "pork"
      case el: Beef    => "beef"
      case el: Chicken => "chicken"
    }
  }
}
