package lection.ready

class Cheburek[A](val fill: A)

object Cheburek {
  implicit def showCheburek[A : Show] = new Show[Cheburek[A]] {
    override def show(el: Cheburek[A]): String =
      s"lection.Cheburek with ${Show[A].show(el.fill)}"
  }
}
