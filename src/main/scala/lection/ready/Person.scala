package lection.ready

import java.time.{Instant, ZoneId}
import java.time.format.DateTimeFormatter

class Person(val name: String, val birthday: Long)

object Person {
  implicit val showPerson = new Show[Person] {

    val datePattern = DateTimeFormatter.ofPattern("dd/MM/yyyy")

    override def show(p: Person): String = s"lection.Person: ${p.name}, brth: " ++
      Instant.ofEpochMilli(p.birthday)
        .atZone(ZoneId.systemDefault())
        .format(datePattern)
  }
}
