package lection.ready

object Sandbox extends App {

  val person = new Person("lection.Person", 123456789000L)

  def println1[A](obj: A)(implicit sh: Show[A]): Unit = Console.println(sh.show(obj))

  println1(person)

  def println2[A : Show](obj: A): Unit = Console.println(implicitly[Show[A]].show(obj))

  import Show._
  def println3[A : Show](obj: A): Unit = Console.println(obj.show)

  println3(person)

  def quad(x: Int) = x * x

  implicit def str2int(str: String) = str.length

  Console.println(quad("сорок два"))

}

