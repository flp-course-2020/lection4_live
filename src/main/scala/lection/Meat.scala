package lection

sealed trait Meat

trait Beef extends Meat

trait Pork extends Meat

trait Chicken extends Meat

object Meat {
  implicit val showMeat = new Show[Meat] {
    override def show(el: Meat): String = el match {
      case _: Chicken => "chicken"
      case _: Pork => "pork"
      case _: Beef => "beef"
      case _: Meat => "meat"
    }
  }
}
