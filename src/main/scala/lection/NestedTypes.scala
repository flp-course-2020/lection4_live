package lection

class UpperType {
  class NestedType
}

object UpperType {
  //Запись UpperType#NestedType означает что тип NestedType является вложенным типом в UpperType
  //И функция принимает экземпляр этого типа
  implicit def nt2dst(nt: UpperType#NestedType): Dst = new Dst
}

class Dst

object NestedTypes extends App {
  //Напрямую создать инстанс NestedType у вас не выйдет, т.к. scala требует, чтобы было проинициализированно
  //значение верхнего типа, т.е

  val _nt = new UpperType#NestedType //не скомпилируется

  val ut = new UpperType
  val nt = new ut.NestedType //а вот так скомпилируется

  val ut2 = new UpperType
  val nt2 = new ut2.NestedType

  //C точки зрения скалы типы ut.NestedType и ut2.NestedType являются различными

  //Но вы можете писать функции, которые в качестве аргумента принимают какой-то конкретный
  //вложенный тип
  def foo(x: ut.NestedType): ut2.NestedType = new ut2.NestedType

  //Либо писать более общие функции
  def bbb(x: UpperType#NestedType, n: Int): UpperType#NestedType =
    if (n % 2 == 0) new ut2.NestedType
    else new ut.NestedType

  //Имплиситы для вложенных типов также ищутся в объектах компаньонах типов, в которых лежат эти вложенные типы
  val example: Dst = new ut.NestedType
}
