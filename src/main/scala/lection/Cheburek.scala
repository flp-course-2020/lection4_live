package lection

class Cheburek[A](val fill: A)

object Cheburek {
  //Используем implicit def чтобы разрешать цепочки неявных вызовов
  implicit def showCheburek[A : Show] = new Show[Cheburek[A]] {
    override def show(el: Cheburek[A]): String =
      s"Cheburek with ${Show[A].show(el.fill)}"
  }
}
