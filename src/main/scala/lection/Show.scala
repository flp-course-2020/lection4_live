package lection

// Определяем класс типов, который строит строковое представление объекта заданного типа
trait Show[A] {
  def show(el: A): String
}

object Show {
  //Вспомогательный метод, который позволяет нам удобнее обращаться к неявным значениям
  def apply[A](implicit sh: Show[A]): Show[A] = sh

  //Расширяем некоторый тип А. В данном случае, чтобы у некоторого элемента типа А,
  //можно было позвать метод `show`, надо явно импортировать этот класс в нужный скоуп
  //import Show._ или import Show.ShowSyntax
  implicit class ShowSyntax[A](val el: A) extends AnyVal {
    def show(implicit sh: Show[A]): String = sh.show(el)
  }
}