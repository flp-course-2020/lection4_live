package lection

object Main extends App {
  import Show._

  //Первый способ написать метод, который принимает неявный параметр
  def println[A](obj: A)(implicit sh: Show[A]) = {
    Console.println(sh.show(obj))
  }

  //Второй способ написать функцию, принимающую неявный параметр
  def println2[A : Show](obj: A) = {
    //В данном случае, т.к. мы не дали никакого имени для неявного значения,
    //нам приходится использовать вспомогательный метод implicitly, который
    //по типу возвращет неявный параметр
    Console.println(implicitly[Show[A]].show(obj))

    //Т.к. для Show мы определили метод apply, то мы можем написать так
    Console.println(Show[A].show(obj))

    //Эта запись эквивалентна следующей записи
    Console.println(Show.apply[A].show(obj))
  }

  def println3[A : Show](obj: A) = {
    //Т.к. у нас есть import.Show в объекте Main, мы можем использовать методы-расширения
    // из имплисит класса ShowSyntax, мы можем определить наш метод вот так
    Console.println(obj.show)
  }

  //В методы, принимающие неявные параметры, можно явно передать параметр :)
  println3(new Person("Aaa", 1L))(Person.showPerson)


  val p = new Person("Vvvvv", 123456789000L)
  println(p)
  println2(p)
  println3(p)

  implicit def str2int(str: String) = str.length

  val cheb: Cheburek[Meat] = new Cheburek[Meat](new Beef{})
  println(cheb)

  //Чтобы понять, как разрешаются цепочки неявных вызовов,
  //опишем явно, что происходит при вызове println(cheb)

  println(cheb)(Cheburek.showCheburek(Meat.showMeat))

}
