package lection

import java.time.format.DateTimeFormatter
import java.time.{Instant, ZoneId}

class Person(val name: String, val birthDay: Long)

object Person {
  //Определяем инстанс тайпкласса Show для Person
  implicit val showPerson = new Show[Person] {
    val formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy")
    override def show(el: Person): String = {
      val brth = Instant.ofEpochMilli(el.birthDay)
        .atZone(ZoneId.systemDefault())
        .format(formatter)

      s"Person(${el.name}, $brth)"
    }
  }
}
